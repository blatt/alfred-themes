# Alfred Themes



A collection of my Alfred 3 Themes.



## Installation

You obviously need the [Alfred 3](https://www.alfredapp.com/) app to use the theme.

Download/clone the repo or download a single theme.   
Double click the *.alfredappearance to import the theme to Alfred.


## Themes
#### eXon Theme
![eXon Theme](screenshots/alfred-theme-eXon.png)


## Contributing

1. Fork it!
2. Create your theme branch: `git checkout -b my-new-theme`
3. Commit your changes: `git commit -am 'Add my-new-theme'`
4. Push to the branch: `git push origin my-new-theme`
5. Submit a merge request 😃


## License

© Distributed under the terms of the [MIT License](https://gitlab.com/blatt/alfred-themes/raw/master/LICENSE).
